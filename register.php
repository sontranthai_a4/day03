<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <style>
        select {
            border: 2px solid blue;
            border-radius: 0;
            box-sizing: border-box;
            background-color: white;
        }
        button {
            border: 2px solid blue;
            border-radius: 15px;
            box-sizing: border-box;
            background-color: green;
            font-size: 20px;
            color: white;
        }
        .container {
            position:fixed;
            top: 50%;
            left: 50%;
            width:30em;
            /* height:18em; */
            margin-top: -9em;
            margin-left: -15em;
            padding: 56px 56px;
            border: 2px solid blue;
            display: flex;
            flex-direction: column;
            gap: 15px;
            align-content: center;
        }
        .row {
            display: flex;
            justify-content: space-between;
            align-items: stretch;
            gap: 15px;
        }
        .col1 {
            display: flex;
            flex: 1;
            padding: 10px 15px;
        }
        .col2 {
            display: flex;
            flex: 3;
            justify-content: flex-start;
        }
        .label {
            background-color: #3385ff;
            border: 2px solid blue;
            color: white;
            font-size: 18px;
            justify-content: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col1 label">Họ và tên</div>
            <div class="col2"><input style="display: block; flex: 1; border: 2px solid blue;" type="text"></div>
        </div>
        <div class="row">
            <div class="col1 label">Giới tính</div>
            <div class="col2">
                <?php
                    $gender = array(
                        0 => "Nam", 
                        1 => "Nữ"
                    );
                    for ($i = 0; $i <= 1; $i++) {
                        echo "<div style=\"margin: auto 0;\">
                        <input style=\"accent-color: blue;\" type=\"radio\" name=\"gender\" id=\"$gender[$i]\">
                            <label for=\"$gender[$i]\">$gender[$i]</label>
                        </div>
                        ";
                    }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col1 label">Phân khoa</div>
            <div class="col2">
                <select>
                    <?php
                        $DSKHOA = array(
                            null => "", 
                            "MAT" => "Khoa học máy tính", 
                            "KDL" => "Khoa học vật liệu");
                        foreach ($DSKHOA as $key => $khoa) {
                            echo "<option>$khoa</option>";
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="row" style="justify-content: center; margin: 25px 0 0 0;">
            <button style="padding: 16px 56px;">Đăng ký</button>
        </div>        
    </div>
</body>
</html>